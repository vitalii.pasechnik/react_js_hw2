import { Component } from 'react';
import classes from './Header.module.scss';
import { ReactComponent as CartSvg } from '../../img/cart-shopping-solid.svg';
import { ReactComponent as FavSvg } from '../../img/heart-solid.svg';
import CartPopup from '../CartPopup/CartPopup';
import PropTypes from 'prop-types';

class Header extends Component {

    constructor() {
        super();

        this.state = {
            isOpenCartPopup: false
        }

        this.handleToggleCartPopup = this.handleToggleCartPopup.bind(this);
        this.disablePopup = this.disablePopup.bind(this);
    }

    handleToggleCartPopup() {
        !this.state.isOpenCartPopup && !!this.props.cart.length ?
            this.setState(prevState => prevState = { ...prevState, isOpenCartPopup: true })
            :
            this.setState(prevState => prevState = { ...prevState, isOpenCartPopup: false });
    }

    disablePopup() {
        this.setState(prevState => prevState = { ...prevState, isOpenCartPopup: false });
    }

    render() {
        const { favorite, cart } = this.props;
        return (
            <div className={classes.header}>
                <div className={classes.container}>
                    <div className={classes.fav_wrapper}>
                        <FavSvg className={classes.heart} />
                        {!!favorite.length && <div className={classes.count}>{favorite.length}</div>}
                    </div>
                    <div className={classes.cart_wrapper} onClick={this.handleToggleCartPopup}>
                        <CartSvg className={classes.cart} />
                        {!!cart.length && <div className={classes.count}>{cart.length}</div>}
                    </div>
                    {!!this.state.isOpenCartPopup && !!cart.length &&
                        < CartPopup products={cart} {...this.props} handleToggleCartPopup={this.handleToggleCartPopup} disablePopup={this.disablePopup} />
                    }
                </div>
            </div>
        )
    }
}

Header.propTypes = {
    favorite: PropTypes.arrayOf(PropTypes.object).isRequired,
    cart: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default Header;