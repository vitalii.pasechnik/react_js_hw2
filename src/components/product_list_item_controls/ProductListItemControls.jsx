import React, { Component } from "react";
import classes from './ProductListItemControls.module.scss';
import { ReactComponent as BasketSVG } from '../../img/basket_icon.svg';
import Button from "../Button/Button";
import cn from 'classnames';
import PropTypes from 'prop-types';

class ProductListItemControls extends Component {

    constructor({ price, openAddToCartModal, id }) {
        super();
        this.price = price;
        this.openAddToCartModal = openAddToCartModal;
        this.id = id;

        this.state = {
            discount: '',
        };

        this.markProductCartBtn = this.markProductCartBtn.bind(this);
    }

    componentDidMount() {
        if (!!this.price.sale) {
            if (this.price.common > 100) {
                this.setState(prevState => prevState = { ...prevState, discount: `${Math.round((this.price.sale - this.price.common) * 100) / 100} грн` })
            } else if (this.price.common <= 100) {
                this.setState(prevState => prevState = { ...prevState, discount: `${Math.round(100 * this.price.sale / this.price.common) - 100}%` })
            }
        }
    }

    markProductCartBtn() {
        return this.props.cart.some((product) => product.id === this.id)
    }

    render() {
        const { title, available } = this.props;

        return (
            <div className={classes.product_list_item_controls}>
                {!!available ?
                    <>
                        <div className={classes.product_price_container}>
                            <div className={classes.sale_price}>
                                {!!this.price.sale ? this.price.sale : this.price.common}
                                <span className={classes.price_currency}> грн</span>
                            </div>
                            <div className={classes.old_price}>
                                <div className={classes.old_integer}>{!!this.price.sale && this.price.common}</div>
                                <div className={classes.discount}>{this.state.discount}</div>
                            </div>
                        </div>
                        <button
                            className={cn(classes.add_to_cart_round, !!this.markProductCartBtn() && classes.added)}
                            title="Додати в кошик"
                            onClick={() => this.openAddToCartModal(title, this.id)}
                        >
                            <BasketSVG className={classes.icon_basket} />
                        </button>
                    </>
                    :
                    <div className={classes.add_to_comment_wrapper}>
                        <div className={classes.btn_tooltip} role="tooltip">
                            <span>Додамо до замовлення, якщо з’явиться до моменту видачі</span>
                        </div>
                        <Button className='productItem_button'>Хочу замовити</Button>
                    </div>
                }
            </div>
        )
    }
}

ProductListItemControls.propTypes = {
    available: PropTypes.bool.isRequired,
    title: PropTypes.string.isRequired,
    price: PropTypes.object.isRequired,
    cart: PropTypes.arrayOf(PropTypes.object).isRequired,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    openAddToCartModal: PropTypes.func,
}

export default ProductListItemControls;




