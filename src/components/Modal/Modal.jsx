import { Component, Fragment } from 'react';
import classes from './Modal.module.scss';
import cn from 'classnames';
import PropTypes from 'prop-types';


class Modal extends Component {

    constructor({ header, text, closeButton, actions, closeModal, modalClassName }) {
        super();
        this.header = header;
        this.text = text;
        this.closeButton = closeButton;
        this.actions = actions;
        this.closeModal = closeModal;
        this.modalClassName = modalClassName;
    }

    render() {
        return (
            <>
                <div className={cn(classes.modal, classes[this.modalClassName])}>
                    <div className={classes.modal__header}>
                        {this.header}
                        {!!this.closeButton && <div className={classes.closeBtn} onClick={this.closeModal}></div>}
                    </div>
                    <div className={classes.modal__body}>
                        {!!this.text && this.text.map((string, i) => <span key={i}>{string}</span>)}
                    </div>
                    {!!this.actions && <div className={classes.modal__actions}>{
                        this.actions.map((el, i) => <Fragment key={i}>{el}</Fragment>)
                    }</div>}
                </div>
                <div className={cn(classes.modal__layout, classes[this.modalClassName])} onClick={this.closeModal}></div>
            </>
        )
    }
}

Modal.propTypes = {
    header: PropTypes.string,
    text: PropTypes.arrayOf(PropTypes.string),
    actions: PropTypes.arrayOf(PropTypes.node),
    closeButton: PropTypes.bool.isRequired,
    closeModal: PropTypes.func.isRequired,
    modalClassName: PropTypes.string.isRequired,
}

export default Modal;