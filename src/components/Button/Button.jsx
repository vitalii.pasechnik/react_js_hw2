import { Component } from 'react';
import classes from './Button.module.scss';
import cn from 'classnames';
import PropTypes from 'prop-types';

class Button extends Component {

    constructor({ children, backgroundColor = '', closeModal, className, onClickFunc }) {
        super();
        this.children = children;
        this.backgroundColor = backgroundColor;
        this.closeModal = closeModal;
        this.className = className;
        this.onClickFunc = onClickFunc;
    }

    render() {
        return (
            <button
                onClick={() => {
                    !!this.closeModal && this.closeModal();
                    !!this.onClickFunc && this.onClickFunc();
                }}
                className={cn(classes.button, classes[this.className])}
                style={{ backgroundColor: this.backgroundColor }}
            >
                {this.children}
            </button >
        )
    }
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    onClickFunc: PropTypes.func,
    closeModal: PropTypes.func,
    className: PropTypes.string.isRequired,
    children: PropTypes.string.isRequired,
}

export default Button;
