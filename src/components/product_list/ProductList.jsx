import { Component } from "react";
import classes from './ProductList.module.scss';
import ProductListItem from "../product_list_item/ProductListItem";
import PropTypes from 'prop-types';

class ProductList extends Component {

    render() {
        return (
            <ul className={classes.product_list} >
                {this.props.products.map((product) =>
                    <ProductListItem key={product.id} {...product} {...this.props} />
                )}
            </ul>
        )
    }
}

ProductList.propTypes = {
    products: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default ProductList;