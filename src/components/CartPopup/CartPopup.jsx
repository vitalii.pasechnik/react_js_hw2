import { Component } from 'react';
import classes from './CartPopup.module.scss';
import Button from '../Button/Button';
import PropTypes from 'prop-types';

class CartPopup extends Component {

    constructor() {
        super();

        this.mergeProducts = this.mergeProducts.bind(this);
        this.calcTotalPrice = this.calcTotalPrice.bind(this);
    }

    mergeProducts() {
        return this.props.products.reduce((acc, el) => {
            const dublicat = acc.findIndex(a => a.id === el.id);
            dublicat > -1 ? acc[dublicat].count++ : acc.push({ count: 1, ...el });
            return acc;
        }, []);
    }

    calcTotalPrice() {
        const arr = this.mergeProducts();
        return arr.reduce((acc, el) => acc += el.count * (el.price.sale || el.price.common), 0);
        // const totalPrice = arr.map(el => el.price.common * el.count).reduce((a, b) => a + b);
    }

    componentWillUnmount() {
        this.props.disablePopup()
    }

    render() {
        const { dellProductFromCart, handleToggleCartPopup } = this.props;

        return (
            <>
                <div className={classes.cart_mini}>
                    <ul className={classes.product_list} >
                        {this.mergeProducts().map((product) =>
                            <li className={classes.product_row} key={product.id}>
                                {!!product.count && <span>{product.count} x</span>}
                                <span>{product.title}</span>
                                <span>{!!product.price.sale ? product.price.sale : product.price.common} грн</span>
                                <div className={classes.closeBtn} onClick={() => dellProductFromCart(product.id)}></div>
                            </li>
                        )}
                        <li><strong>Загальна сума: {this.calcTotalPrice()} грн</strong></li>
                    </ul>
                    <Button backgroundColor='rgba(255, 255, 255, 0.25)' className='enterCartBtn'>Перейти до кошика</Button>
                </div >
                <div className={classes.layout} onClick={() => handleToggleCartPopup()}></div>
            </>
        )
    }
}

CartPopup.propTypes = {
    dellProductFromCart: PropTypes.func.isRequired,
    handleToggleCartPopup: PropTypes.func.isRequired,
    disablePopup: PropTypes.func.isRequired,
    products: PropTypes.arrayOf(PropTypes.object).isRequired
}


export default CartPopup;