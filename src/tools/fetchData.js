import axios from 'axios';

const fetchData = async () => {
    try {
        const response = await axios.get('http://localhost:3000/db.json');
        if (response.status === 200) {
            return response.data;
        } else {
            throw new Error('Network Error');
        }
    } catch (error) {
        console.error('Fetching Error:', error);
        return;
    }
}

export default fetchData;
