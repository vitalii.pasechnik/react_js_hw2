import { Component } from 'react';
import { Button, Modal, Header } from './components';
import './App.scss';
import ProductList from './components/product_list/ProductList.jsx';
import fetchData from './tools/fetchData';

class App extends Component {

    constructor() {
        super();

        this.headerTxt = '';
        this.bodyTxt = [];
        this.modalClassName = '';
        this.closeButton = false;
        this.targetProductID = null;

        this.state = {
            isOpenModal: false,
            products: [],
            cart: [],
            favorite: [],
        };

        this.closeModal = this.closeModal.bind(this);
        this.openAddToCartModal = this.openAddToCartModal.bind(this);
        this.addProductsToCart = this.addProductsToCart.bind(this);
        this.addProductsToFavorite = this.addProductsToFavorite.bind(this);
        this.dellProductFromCart = this.dellProductFromCart.bind(this);
    }

    async componentDidMount() {
        const data = await fetchData();
        this.setState(prevState => prevState = { ...prevState, products: data });
        this.setState({ cart: JSON.parse(localStorage.getItem('cart')) || [] });
        this.setState({ favorite: JSON.parse(localStorage.getItem('favorite')) || [] });
    }

    componentDidUpdate() {
        localStorage.cart = JSON.stringify(this.state.cart);
        localStorage.favorite = JSON.stringify(this.state.favorite);
    }

    openModal() {
        this.setState(prevState => prevState = { ...prevState, isOpenModal: true });
        this.closeButton = true;
    }

    closeModal() {
        this.setState(prevState => prevState = { ...prevState, isOpenModal: false });
    }

    openAddToCartModal(title, id) {
        this.headerTxt = title;
        this.targetProductID = id;
        this.bodyTxt = [`Бажаєте додати '${title}'`, `до кошика?`];
        this.modalClassName = 'addToCartModal';
        this.openModal();
    }

    addProductsToFavorite(action, id) {
        action === '+' && this.state.products.forEach(product => product.id === id && this.setState(prevState => prevState = { ...prevState, favorite: [...prevState.favorite, product] }));
        action === '-' && this.setState(prevState => prevState = { ...prevState, favorite: prevState.favorite.filter(product => product.id !== id) });
    }

    addProductsToCart() {
        this.state.products.forEach(product => product.id === this.targetProductID && this.setState(prevState => prevState = { ...prevState, cart: [...prevState.cart, product] }))
    }

    dellProductFromCart(id) {
        this.setState(prevState => prevState = { ...prevState, cart: prevState.cart.filter(product => product.id !== id) });
    }

    render() {
        // console.log(this.state.products);
        return (
            <div className="App">
                <Header
                    cart={this.state.cart}
                    favorite={this.state.favorite}
                    dellProductFromCart={this.dellProductFromCart}
                />
                <div className="container">
                    <ProductList
                        products={this.state.products}
                        favorite={this.state.favorite}
                        cart={this.state.cart}
                        openAddToCartModal={this.openAddToCartModal}
                        addProductsToFavorite={this.addProductsToFavorite}
                    />
                    {!!this.state.isOpenModal &&
                        <Modal
                            closeModal={this.closeModal}
                            modalClassName={this.modalClassName}
                            header={this.headerTxt}
                            text={this.bodyTxt}
                            closeButton={this.closeButton}
                            actions={[
                                <Button closeModal={this.closeModal} className={this.modalClassName} onClickFunc={this.addProductsToCart}>OK</Button>,
                                <Button closeModal={this.closeModal} className={this.modalClassName}>Скасувати</Button>
                            ]}
                        />
                    }
                </div>
            </div >
        )
    }
}

export default App;
